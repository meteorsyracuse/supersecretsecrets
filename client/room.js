Meteor.subscribe('users');

Template.room.onRendered(function () {
    console.log('rendered template');

    //save link to clipboard
    ZeroClipboard.config({swfPath: Meteor.absoluteUrl() + "ZeroClipboard.swf"});
    var client = new ZeroClipboard($(".copyUrl"));
    client.on("ready", function (readyEvent) {
        client.on("copy", function (event) {
            var clipboard = event.clipboardData;
            clipboard.setData('text/plain', $('#roomUrl').val());
        });
        client.on("aftercopy", function (event) {
            // `this` === `client`
            // `event.target` === the element that was clicked
            //event.target.style.display = "none";
            //alert("Copied text to clipboard: " + event.data["text/plain"] );
            $(event.target).children('.glyphicon-ok').removeClass('hidden');
            $(event.target).children('.glyphicon-copy').addClass('hidden');
        });
    });
    $('.messages')[0].scrollTop = $('.messages')[0].scrollHeight;

//    Tracker.autorun(function(){
//        var roomId = Router.current().params._id;
//        if(!Rooms.findOne(roomId)){
//            Router.go('index');
//        }
//    })
});

Template.room.helpers({
    messages: function (room) {
        return room.roomMessages();
    },
    countdown: function (room) {
        return moment.utc(room.timeRemaining * 1000).format("m:ss");
    },
    runningOut: function (room) {
        if ( room.timeRemaining < 30) {
            return "";
        } else {
            return "hidden";
        }
    },
    isMine: function (userId) {
        if (userId === Meteor.userId()) {
            return 'myMessage';
        }
    },
    timeAgo: function (message) {
        return moment(message.createdAt).fromNow();
    }
});

Template.room.events({
    'submit .newMessage': function (e, t) {
        e.preventDefault();
        this.set('timeRemaining', 120);
        //console.log(e, t, this);
        var message = new Message({
            owner: Meteor.userId(),
            text: e.target.messageText.value,
            room: this._id
        });
        this.save();
        message.save();
        $('.messageText').val('');
        $('.messages')[0].scrollTop = $('.messages')[0].scrollHeight;
    }
});