Meteor.subscribe('users');

Template.index.helpers({
});

Template.photoGrid.onRendered(function (e) {
	console.log('rendered yo ');
	$('.img-circle-test').each(function (i) {
		var current = this;
		Meteor.setTimeout(function (e) {
			console.log('i is ', i);
			$(current).removeClass('hideMe');
			$(current).addClass('animated zoomInDown image-box-shadow');
		}, i * 200);
	});
});

Template.index.events({
	'click .createRoom': function(e,t){
		e.preventDefault();
		var room = new Room({owner: Meteor.userId()});
		var roomId = room.save();
		console.log(roomId);
		Router.go('room', {_id: roomId});
	},
	'click .avatar': function(e,t){
		console.log(e);
		var avatarURL = e.target.dataset.url;
		var me = Meteor.user();
		me.set('avatarURL', avatarURL);
		Meteor.call('saveUser', me);
	}
});
