Router.configure({
	layoutTemplate: 'mainLayout',
	notFoundTemplate: 'notFound'
});

Router.route('/', {
	name: 'index'
});

Router.route('/room/:_id', {
	name: 'room',
	data: function(){
		var find = Rooms.findOne(this.params._id);
		console.log('find', find);
		return find;
	}
});

Router.onBeforeAction('dataNotFound', {only: 'room'});