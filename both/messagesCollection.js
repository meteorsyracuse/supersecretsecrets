Messages = new Mongo.Collection('messages');

Message = Astro.Class({
    name: 'Message',
    collection: Messages,
    fields: {
        owner: 'string',
        room: 'string',
        text: 'string'
    },
    behaviors: {
        timestamp: {
            created: 'created'
        }
    },
    events: {
        afterSave: function (e) {

            var message = this;
            var messageId = e.target._id;

            //delete message after 6 seconds
            console.log('deleting soon');
            _.delay(function () {
                $('.' + messageId).animate({height: 'toggle', opacity: 'toggle'}, 'slow');
                Meteor.setTimeout(function () {
                    message.remove();
                }, 650);
            }, 6000);

            //give room the latest message timestamp, so it will self-delete properly
            var room = Rooms.findOne(message.room);
            console.log(message.created);
            room.set({'mostRecentMessage': message._id, 'lastMessageTime': new Date()});
            room.save();
        }
    },
    methods: {
        messageOwner: function () {
            return Meteor.users.findOne(this.owner);
        }
    }
});