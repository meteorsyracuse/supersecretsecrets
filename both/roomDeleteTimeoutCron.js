Meteor.methods({
    startCounting: function (roomId) {
        if (Meteor.isServer) {
            SyncedCron.add({
                name: roomId,
                schedule: function (parser) {
                    // parser is a later.parse object
                    return parser.text('every 1 second');
                },
                job: function(){
                    var room = Rooms.findOne(roomId);
                    room.set('timeRemaining', room.timeRemaining - 1);
                    room.save();
                    if(room.timeRemaining < 1){
                        SyncedCron.remove(roomId);
                        room.remove();
                        //console.log('cron should end now');
                    }
                }
            });
            SyncedCron.start();
        }
    }
});