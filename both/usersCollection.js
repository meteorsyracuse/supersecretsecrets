User = Astro.Class({
    name: 'User',
    collection: Meteor.users,
    fields: {
        emails: 'array',
        profile: 'object',
        avatarURL: 'string',
        services: 'object',
        preferences: 'object',
        createdAt: 'date',
        username: 'string'
    }
});

if(Meteor.isServer){
    Meteor.publish('users', function(){
        return Meteor.users.find({}, {fields: {avatarURL: 1, emails: 1, username: 1}});
    })
}