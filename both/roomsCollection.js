Rooms = new Mongo.Collection('rooms');

Room = Astro.Class({
    name: 'Room',
    collection: Rooms,
    fields: {
        timeRemaining: {
            type: 'number',
            optional: true,
            default: function () {
                return 120;
            }
        },
        roomName: 'string',
        owner: 'string',
        mostRecentMessage: 'string',
        lastMessageTime: 'date',
        participants: {
            type: 'array',
            default: function () {
                if (Meteor.isServer) {
                    return [this.userId];
                }
            },
            optional: true
        }
    },
    events: {
        afterInsert: function (e) {
            console.log(this);
            Meteor.call('startCounting', this._id);
        },
        afterChange: function (e) {
            //if there are no more participants, delete it
            if (!this.participants.length) {
                this.remove();
            }
        },
        afterRemove: function (e) {
            //remove messages linked to this room
            Messages.find({room: this._id}).forEach(function (message) {
                message.remove();
            });
            if(Meteor.isClient){
                Router.go('index');
            }
        }
    },
    methods: {
        roomMessages: function () {
            return Messages.find({room: this._id}, {sort: {createdAt: 1}});
        },
        roomUrl: function () {
            var url = Meteor.absoluteUrl() + 'room/' + this._id;
            return url;
        }
    }
});
